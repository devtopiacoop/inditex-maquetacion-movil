# ANTES DE EMPEZAR #

### Cómo está montado esto: ###

(Si no está así aún, es que estáis leyendo esto muy pronto)


En la carpeta `styles` encontramos tanto los archivo `.less` como los archivos `.css` pero solo debemos editar los `.less`.
Si un color, tamaño, fuente, o lo que sea se va a utilizar más de una vez, se mueve al archivo de valores correspondiente (colors, dimensions, fonts, etc).
Utilizar mixins y variables para tener un archivo de estilos lo más claro posible
Comentar el por qué de ciertas acciones en el less (no saldrán en el css) para facilitar la modificación posterior por parte de otro desarrollador.