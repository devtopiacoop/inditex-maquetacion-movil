
// Custom combobox
(function( $ ) {
  $.widget( "custom.combobox", {
    _create: function() {
      this.wrapper = $( "<span>" )
        .addClass( "custom-combobox" )
        .addClass(this.element.attr('class').replace('combobox',''))
        .insertAfter( this.element );

      this.element.hide();
      this._createAutocomplete();
      this._createShowAllButton();
    },

    _createAutocomplete: function() {
      var selected = this.element.children( ":selected" ),
        value = selected.val() ? selected.text() : "";

      this.input = $( "<input>" )
        .appendTo( this.wrapper )
        .val( value )
        .attr( "title", "" )
        .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
        .autocomplete({
          delay: 0,
          minLength: 0,
          source: $.proxy( this, "_source" )
        })
        .tooltip({
          tooltipClass: "ui-state-highlight"
        });

      this._on( this.input, {
        autocompleteselect: function( event, ui ) {
          ui.item.option.selected = true;
          this._trigger( "select", event, {
            item: ui.item.option
          });
        },

        autocompletechange: "_removeIfInvalid"
      });
    },

    _createShowAllButton: function() {
      var input = this.input,
        wasOpen = false;

      $( "<a>" )
        .attr( "tabIndex", -1 )
        .attr( "title", "Show All Items" )
        .tooltip()
        .appendTo( this.wrapper )
        .button({
          icons: {
            primary: "ui-icon-triangle-1-s"
          },
          text: false
        })
        .removeClass( "ui-corner-all" )
        .addClass( "custom-combobox-toggle ui-corner-right" )
        .mousedown(function() {
          wasOpen = input.autocomplete( "widget" ).is( ":visible" );
        })
        .click(function() {
          input.focus();

          // Close if already visible
          if ( wasOpen ) {
            return;
          }

          // Pass empty string as value to search for, displaying all results
          input.autocomplete( "search", "" );
        });
    },

    _source: function( request, response ) {
      var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
      response( this.element.children( "option" ).map(function() {
        var text = $( this ).text();
        if ( this.value && ( !request.term || matcher.test(text) ) )
          return {
            label: text,
            value: text,
            option: this
          };
      }) );
    },

    _removeIfInvalid: function( event, ui ) {

      // Selected an item, nothing to do
      if ( ui.item ) {
        return;
      }

      // Search for a match (case-insensitive)
      var value = this.input.val(),
        valueLowerCase = value.toLowerCase(),
        valid = false;
      this.element.children( "option" ).each(function() {
        if ( $( this ).text().toLowerCase() === valueLowerCase ) {
          this.selected = valid = true;
          return false;
        }
      });

      // Found a match, nothing to do
      if ( valid ) {
        return;
      }

      // Remove invalid value
      this.input
        .val( "" )
        .attr( "title", value + " didn't match any item" )
        .tooltip( "open" );
      this.element.val( "" );
      this._delay(function() {
        this.input.tooltip( "close" ).attr( "title", "" );
      }, 2500 );
      this.input.autocomplete( "instance" ).term = "";
    },

    _destroy: function() {
      this.wrapper.remove();
      this.element.show();
    }
  });
})( jQuery );


// Custom select popup
(function( $ ) {
  $.widget( "custom.selectPopup", {
    _create: function() {
      _self = this;
      this.wrapper = $( "<span>" )
        .addClass( "custom-select-popup" )
        .addClass(this.element.attr('class').replace('select-popup',''))
        .insertAfter( this.element );

      this.element.hide();
      this._createToggleButton();

      // Close handler
      $("#select-popup-back").click( function() {
        $("#select-popup").hide();
      } );

    },

    _createToggleButton: function() {
      var wasOpen = false;

      this.button = $( "<a>" )
        .attr( "tabIndex", -1 )
        .text($("option:selected", this.element).text())
        .appendTo( this.wrapper )
        .button()
        .removeClass( "ui-corner-all ui-button ui-state-default ui-button-text-only" )
        .addClass( "select-popup-button" )
        .on( "click", {
          selectPopup: this
        }, function(event) {

              selectPopup = event.data.selectPopup;

              // Set panel title
              $('#select-options-title').text( selectPopup.element.prev( ".label" ).text().replace('*','') );

              // Empty panel list
              $('#select-popup-options').empty();

              // Add and LI for each option
              $("option", selectPopup.element).each(function() {
                $("#select-popup-options").append('<li class="menu_item"><a href="#" data-value="'+this.value+'"><div class="option">'+this.text+'</div></a></li>');
              });

              // Add click handler to each element in list
              $("#select-popup-options a").on( "click", {
                  selectPopup: event.data.selectPopup
                }, function(e) {
                  selectPopup = e.data.selectPopup;
                  value = $(this).attr("data-value");
                  selectPopup.element.val(value);
                  selectPopup.element.change();
                  $("#select-popup").toggle();
                  selectPopup.element.focus();

              });

              $("#select-popup").toggle();
              window.scrollTo(0, 0);

      });

      // Add change behavior
      this.element.on( "change", {
          selectPopup: this
        }, function(e) {
          selectPopup.button.text($('option[value="'+selectPopup.element.val()+'"]', selectPopup.element).text())
      });

    },

    _destroy: function() {
      this.wrapper.remove();
      this.element.show();
    }
  });
})( jQuery );

// Replace selects by combobox and select-popup
$(function() {
  $( ".combobox" ).combobox();
  $( "select.select-popup" ).selectPopup();
  $( "#toggle" ).click(function() {
    $( "#combobox" ).toggle();
  });
});


// Replace datepickers
$(function() {
  $( ".datepicker" ).datepicker();
});
